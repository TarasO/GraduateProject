package com.oliinyk.medkit.util

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.time.LocalDateTime

class GsonLocalDateTimeAdapter : JsonDeserializer<LocalDateTime> {
    override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
        ): LocalDateTime {
            val array = json!!.asJsonArray
            return LocalDateTime.of(array.get(0).asInt, array.get(1).asInt, array.get(2).asInt,
                array.get(3).asInt, array.get(4).asInt, array.get(5).asInt)
    }
}