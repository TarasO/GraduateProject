package com.oliinyk.medkit.util;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MapUtils {
    public static double latAddDistance(double latitude, double distance) {
        return latitude + (distance * (1 / ((2 * Math.PI / 360) * 6378.137)) / 1000);
    }

    public static double lonAddDistance(double longitude, double latitude, double distance) {
        return longitude + ((distance * (1 / ((2 * Math.PI / 360) * 6378.137)) / 1000) / Math.cos(latitude * (Math.PI / 180)));
    }

    public static List<MarkerOptions> jsonToPlaces(String body) throws JSONException {
        List<MarkerOptions> markers = new ArrayList<>();

        JSONObject jObject = new JSONObject(body);
        JSONArray jArray = jObject.getJSONArray("elements");

        for (int i = 0; i < jArray.length(); i++) {
            JSONObject place = jArray.getJSONObject(i);
            markers.add(jsonToPlace(place));
        }

        return markers;
    }

    public static JSONArray jsonToJArray(String body) throws JSONException {
        return new JSONObject(body).getJSONArray("elements");
    }

    public static MarkerOptions jsonToPlace(JSONObject place) throws JSONException {
        double latitude = place.getDouble("lat");
        double longitude = place.getDouble("lon");

        String name;
        try {
            name = place.getJSONObject("tags").getString("name");
        } catch (JSONException e) {
            name = "Аптека";
        }

        return new MarkerOptions().position(new LatLng(latitude, longitude))
                .title(name);
    }

    public static Map<Double[], String> jsonToLatLng(String body) throws JSONException {
        Map<Double[], String> locations = new LinkedHashMap<>();

        JSONObject jObject = new JSONObject(body);
        JSONArray jArray = jObject.getJSONArray("elements");

        for (int i = 0; i < jArray.length(); i++) {
            JSONObject place = jArray.getJSONObject(i);
            double latitude = place.getDouble("lat");
            double longitude = place.getDouble("lon");

            String name;
            try {
                name = place.getJSONObject("tags").getString("name");
            } catch (JSONException e) {
                name = "Аптека";
            }

            locations.put(new Double[]{longitude, latitude}, name);
        }

        return locations;
    }

    public static Map<Double[], Double> jsonToDistances(String body) throws JSONException {
        Map<Double[], Double> distances = new HashMap<>();

        JSONObject jObject = new JSONObject(body);
        JSONArray jArray = jObject.getJSONArray("destinations");

        for (int i = 0; i < jArray.length()-1; i++) {
            JSONObject places = jArray.getJSONObject(i);
            JSONArray location = places.getJSONArray("location");
            double distance = places.getDouble("snapped_distance");
            distances.put(new Double[]{location.getDouble(0), location.getDouble(1)},
                    distance);
        }

        return distances;
    }

    public static List<LatLng> jsonToDirections(String body) throws JSONException {
        List<LatLng> directions = new ArrayList<>();

        JSONObject jObject = new JSONObject(body);
        JSONArray coordinates = jObject.getJSONArray("features")
                                       .getJSONObject(0)
                                       .getJSONObject("geometry")
                                       .getJSONArray("coordinates");

        for(int i = 0; i < coordinates.length(); i++) {
            JSONArray latLng = coordinates.getJSONArray(i);
            directions.add(new LatLng(latLng.getDouble(1), latLng.getDouble(0)));
        }

        return directions;
    }
}
