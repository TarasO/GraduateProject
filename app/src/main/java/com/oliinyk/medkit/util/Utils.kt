package com.oliinyk.medkit.util

import com.oliinyk.medkit.model.MedicineState
import com.oliinyk.medkit.model.MedicineState.*
import java.time.LocalDate

class Utils {

    companion object {
        fun getMedicineState(expirationDate: LocalDate): MedicineState {
            val now = LocalDate.now()
            return if (now.isAfter(expirationDate.minusDays(7))
                && now.isBefore(expirationDate) || now.isEqual(expirationDate)
            ) {
                EXPIRING
            } else if (now.isAfter(expirationDate)) {
                EXPIRED
            } else {
                STOCKED
            }
        }
    }
}