package com.oliinyk.medkit

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.oliinyk.medkit.converters.LocalDateConverter
import com.oliinyk.medkit.converters.LocalDateTimeConverter
import com.oliinyk.medkit.dao.*
import com.oliinyk.medkit.model.*
import com.oliinyk.medkit.model.tuple.MedicineInfo
import com.oliinyk.medkit.model.tuple.MedicineWithExpiration

@Database(
    version = 1,
    entities = [
        Country::class,
        Producer::class,
        Category::class,
        CategoryMedicine::class,
        ReleaseForm::class,
        Medicine::class,
        MedicineActiveSubstance::class,
        ActiveSubstance::class,
        Note::class,
        MedkitSet::class,
        SetMedicine::class
    ],
    views = [
        MedicineWithExpiration::class,
        MedicineInfo::class
    ],
    exportSchema = false
)
@TypeConverters(
    LocalDateConverter::class,
    LocalDateTimeConverter::class
)
abstract class MedkitDatabase : RoomDatabase() {
    abstract fun countryDao(): CountryDAO
    abstract fun producerDao(): ProducerDAO
    abstract fun categoryDao(): CategoryDAO
    abstract fun categoryMedicineDao(): CategoryMedicineDAO
    abstract fun releaseFormDao(): ReleaseFormDAO
    abstract fun medicineDao(): MedicineDAO
    abstract fun medicineActiveSubstanceDao(): MedicineActiveSubstanceDAO
    abstract fun activeSubstanceDao(): ActiveSubstanceDAO
    abstract fun noteDao(): NoteDAO
    abstract fun medkitSetDao(): MedkitSetDAO
    abstract fun setMedicineDao(): SetMedicineDAO

    companion object {
        private var INSTANCE: MedkitDatabase? = null
        fun buildDatabase(context: Context) {
            if(INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(context,
                                                    MedkitDatabase::class.java,
                                              "medkit")
                                   .build()
                }
            }
        }
        fun getDatabase(): MedkitDatabase {
            return INSTANCE!!
        }
        fun getDatabase(context: Context): MedkitDatabase {
            if(INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(context,
                        MedkitDatabase::class.java,
                        "medkit")
                        .build()
                }
            }
            return INSTANCE!!
        }
    }
}