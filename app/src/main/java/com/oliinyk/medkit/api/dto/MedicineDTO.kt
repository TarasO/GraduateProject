package com.oliinyk.medkit.api.dto

import java.time.LocalDateTime

data class MedicineDTO(
    val id: Long,
    val name: String,
    val inn: String,
    val expirationPeriod: Int,
    val byRecipe: Boolean,
    val registrationNumber: String,
    val manualUrl: String?,
    val producer: ProducerDTO,
    val releaseForm: ReleaseFormDTO,
    val categories: List<CategoryDTO>,
    val activeSubstances: List<ActiveSubstanceDTO>,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime?
)