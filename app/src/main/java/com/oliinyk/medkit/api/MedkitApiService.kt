package com.oliinyk.medkit.api

import com.oliinyk.medkit.api.dto.*
import retrofit2.Call
import retrofit2.http.GET

interface MedkitApiService {

    @GET("countries")
    fun getCountries(): Call<List<CountryDTO>>

    @GET("producers")
    fun getProducers(): Call<List<ProducerDTO>>

    @GET("release-forms")
    fun getReleaseForms(): Call<List<ReleaseFormDTO>>

    @GET("categories")
    fun getCategories(): Call<List<CategoryDTO>>

    @GET("active-substances")
    fun getActiveSubstances(): Call<List<ActiveSubstanceDTO>>

    @GET("medicines")
    fun getMedicines(): Call<List<MedicineDTO>>
}