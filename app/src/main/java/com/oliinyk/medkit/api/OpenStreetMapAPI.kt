package com.oliinyk.medkit.api

import android.location.Location
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response

object OpenStreetMapAPI {
    private const val routesKey = "5b3ce3597851110001cf6248882360f9f3b54b71844cb447766acc0e"

    private val client = OkHttpClient()

    public fun findPharmaciesAround(s: Double, w: Double, n: Double, e: Double): Response {
        val body = """
                        <osm-script output="json" output-config="" timeout="25">
                          <union into="_">
                            <query into="_" type="node">
                              <has-kv k="amenity" modv="" v="pharmacy"/>
                              <bbox-query s="$s" w="$w" n="$n" e="$e"/>
                            </query>
                          </union>
                          <print e="" from="_" geometry="skeleton" ids="yes" limit="" mode="body" n="" order="id" s="" w=""/>
                          <recurse from="_" into="_" type="down"/>
                          <print e="" from="_" geometry="skeleton" ids="yes" limit="" mode="skeleton" n="" order="quadtile" s="" w=""/>
                        </osm-script>
                    """.trimIndent().toRequestBody("application/xml".toMediaType())

        val request: Request = Request.Builder()
            .url("https://lz4.overpass-api.de/api/interpreter")
            .post(body)
            .addHeader("Content-type", "application/xml")
            .addHeader("Accept", "application/json")
            .build()

        return client.newCall(request).execute()
    }

    public fun getDistanceMatrix(locations: MutableMap<Array<Double>, String>?): Response {
        val body = """
                      {
                        "locations": ${locations!!.keys.toTypedArray().contentDeepToString()},
                        "sources": [${locations.size-1}],
                        "metrics": ["distance"],
                        "units": "m"
                      }
                   """.trimIndent().toRequestBody("application/json".toMediaType())

        val request: Request = Request.Builder()
            .url("https://api.openrouteservice.org/v2/matrix/driving-car")
            .post(body)
            .addHeader("Authorization", routesKey)
            .addHeader("Content-type", "application/json")
            .addHeader("Accept", "application/json")
            .build()

        return client.newCall(request).execute()
    }

    public fun getDirections(location: Location, closestLngLat: Array<Double>): Response {
        val request: Request = Request.Builder()
            .url("https://api.openrouteservice.org/v2/directions/driving-car?api_key=$routesKey" +
                    "&start=${location.longitude},${location.latitude}&end=${closestLngLat[0]}," +
                    "${closestLngLat[1]}")
            .build()

        return client.newCall(request).execute()
    }
}
