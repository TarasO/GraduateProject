package com.oliinyk.medkit.api.dto

import java.time.LocalDateTime

data class CountryDTO(
    val id: Long,
    val name: String,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime?
)
