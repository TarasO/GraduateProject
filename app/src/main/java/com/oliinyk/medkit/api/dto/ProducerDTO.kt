package com.oliinyk.medkit.api.dto

import java.time.LocalDateTime

data class ProducerDTO(
    val id: Long,
    val name: String,
    val address: String?,
    val country: CountryDTO,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime?
)
