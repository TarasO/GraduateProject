package com.oliinyk.medkit.api.dto

import java.time.LocalDateTime

data class ActiveSubstanceDTO (
    val id: Long,
    val name: String,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime?
)