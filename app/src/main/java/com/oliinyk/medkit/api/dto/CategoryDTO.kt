package com.oliinyk.medkit.api.dto

import java.time.LocalDateTime

data class CategoryDTO (
    val id: Long,
    val name: String,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime?
)