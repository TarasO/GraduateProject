package com.oliinyk.medkit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.GsonBuilder
import com.oliinyk.medkit.api.MedkitApiService
import com.oliinyk.medkit.model.*
import com.oliinyk.medkit.util.GsonLocalDateTimeAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.SocketTimeoutException
import java.time.LocalDateTime

class MainActivityViewModel : ViewModel() {
    private val countryDao = MedkitDatabase.getDatabase().countryDao()
    private val producerDao = MedkitDatabase.getDatabase().producerDao()
    private val substanceDao = MedkitDatabase.getDatabase().activeSubstanceDao()
    private val categoryDao = MedkitDatabase.getDatabase().categoryDao()
    private val releaseFormDao = MedkitDatabase.getDatabase().releaseFormDao()
    private val medicineDao = MedkitDatabase.getDatabase().medicineDao()
    private val categoryMedicineDao = MedkitDatabase.getDatabase().categoryMedicineDao()
    private val medicineActiveSubstanceDAO = MedkitDatabase.getDatabase().medicineActiveSubstanceDao()

    private val _isEmpty = MutableLiveData<Boolean>()
    val isEmpty: LiveData<Boolean> = _isEmpty

    private val _connectionError = MutableLiveData(false)
    val connectionError: LiveData<Boolean> = _connectionError

    init {
        val gson = GsonBuilder().registerTypeAdapter(LocalDateTime::class.java,
            GsonLocalDateTimeAdapter())
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.0.2.2:8080/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        val service = retrofit.create(MedkitApiService::class.java)

        viewModelScope.launch(Dispatchers.IO) {
            val empty = countryDao.count() == 0
            _isEmpty.postValue(empty)
            if(empty) {
                try {
                    val countries = service.getCountries().execute().body()!!
                        .map { x -> Country(x.id, x.name, x.createdAt, x.updatedAt) }
                    countryDao.insertAll(*countries.toTypedArray())

                    val producers = service.getProducers()
                        .execute().body()!!
                        .map { x ->
                            Producer(
                                x.id,
                                x.name,
                                x.address,
                                x.country.id,
                                x.createdAt,
                                x.updatedAt
                            )
                        }
                    producerDao.insertAll(*producers.toTypedArray())

                    val substances = service.getActiveSubstances()
                        .execute().body()!!
                        .map { x -> ActiveSubstance(x.id, x.name, x.createdAt, x.updatedAt) }
                    substanceDao.insertAll(*substances.toTypedArray())

                    val categories = service.getCategories()
                        .execute().body()!!
                        .map { x -> Category(x.id, x.name, x.createdAt, x.updatedAt) }
                    categoryDao.insertAll(*categories.toTypedArray())

                    val releaseForms = service.getReleaseForms()
                        .execute().body()!!
                        .map { x -> ReleaseForm(x.id, x.name, x.createdAt, x.updatedAt) }
                    releaseFormDao.insertAll(*releaseForms.toTypedArray())

                    service.getMedicines()
                        .execute().body()!!
                        .forEach {
                            val medicine = Medicine(
                                it.id, it.name, it.inn, it.expirationPeriod,
                                it.byRecipe, it.registrationNumber, it.manualUrl,
                                it.producer.id, it.releaseForm.id, it.createdAt, it.updatedAt
                            )
                            medicineDao.insertAll(medicine)
                            val mSubstances = it.activeSubstances.map { x ->
                                MedicineActiveSubstance(
                                    it.id,
                                    x.id
                                )
                            }
                            medicineActiveSubstanceDAO.insertAll(*mSubstances.toTypedArray())

                            val mCategories =
                                it.categories.map { x -> CategoryMedicine(it.id, x.id) }
                            categoryMedicineDao.insertAll(*mCategories.toTypedArray())
                        }

                    _isEmpty.postValue(false)
                } catch (e: SocketTimeoutException) {
                    _connectionError.postValue(true)
                }
            }
        }
    }
}