package com.oliinyk.medkit.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.oliinyk.medkit.model.MedkitSet
import com.oliinyk.medkit.model.SetType
import com.oliinyk.medkit.model.relationship.SetWithMedicines

@Dao
interface MedkitSetDAO {

    @Insert
    fun insert(sets: MedkitSet): Long

    @Insert
    fun insertAll(vararg sets: MedkitSet)

    @Update
    fun updateSet(set: MedkitSet)

    @Transaction
    @Query("SELECT * FROM MedkitSet WHERE setType = :setType")
    fun getSetsWithMedicines(setType: SetType): LiveData<List<SetWithMedicines>>

    @Delete
    fun deleteSet(set: MedkitSet)
}