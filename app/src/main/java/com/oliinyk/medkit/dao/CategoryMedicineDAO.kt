package com.oliinyk.medkit.dao

import androidx.room.Dao
import androidx.room.Insert
import com.oliinyk.medkit.model.CategoryMedicine

@Dao
interface CategoryMedicineDAO {

    @Insert
    fun insertAll(vararg categoryMedicines: CategoryMedicine)
}