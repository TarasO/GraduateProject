package com.oliinyk.medkit.dao

import androidx.room.*
import com.oliinyk.medkit.model.Note

@Dao
interface NoteDAO {

    @Insert
    fun insertAll(vararg notes: Note)

    @Update
    fun update(note: Note)

    @Query("SELECT EXISTS(SELECT 1 FROM Note n WHERE n.medicineId = :medicineId)")
    fun hasNotesByMedicineId(medicineId: Long): Boolean

    @Transaction
    @Query("DELETE FROM Note WHERE medicineId = :medicineId")
    fun deleteByMedicineId(medicineId: Long)

    @Delete
    fun delete(note: Note)
}