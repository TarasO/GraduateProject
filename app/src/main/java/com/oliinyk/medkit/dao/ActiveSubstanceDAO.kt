package com.oliinyk.medkit.dao

import androidx.room.Dao
import androidx.room.Insert
import com.oliinyk.medkit.model.ActiveSubstance

@Dao
interface ActiveSubstanceDAO {

    @Insert
    fun insertAll(vararg substances: ActiveSubstance)
}