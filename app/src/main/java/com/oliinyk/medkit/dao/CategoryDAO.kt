package com.oliinyk.medkit.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.oliinyk.medkit.model.Category
import com.oliinyk.medkit.model.tuple.CategoryNameTuple

@Dao
interface CategoryDAO {

    @Insert
    fun insertAll(vararg categories: Category)

    @Transaction
    @Query("SELECT id, name FROM Category")
    fun findNames(): LiveData<List<CategoryNameTuple>>
}