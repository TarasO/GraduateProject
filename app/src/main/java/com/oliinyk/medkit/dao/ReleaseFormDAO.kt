package com.oliinyk.medkit.dao

import androidx.room.Dao
import androidx.room.Insert
import com.oliinyk.medkit.model.ReleaseForm

@Dao
interface ReleaseFormDAO {

    @Insert
    fun insertAll(vararg releaseForms: ReleaseForm)
}