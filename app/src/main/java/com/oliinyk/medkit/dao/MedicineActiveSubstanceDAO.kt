package com.oliinyk.medkit.dao

import androidx.room.Dao
import androidx.room.Insert
import com.oliinyk.medkit.model.MedicineActiveSubstance

@Dao
interface MedicineActiveSubstanceDAO {

    @Insert
    fun insertAll(vararg medicineSubstances: MedicineActiveSubstance)
}