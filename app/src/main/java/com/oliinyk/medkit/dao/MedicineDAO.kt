package com.oliinyk.medkit.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.oliinyk.medkit.model.Medicine
import com.oliinyk.medkit.model.relationship.MedicineWithEverything
import com.oliinyk.medkit.model.relationship.MedicineWithNotes
import com.oliinyk.medkit.model.relationship.MedicineWithReleaseFormAndProducer

@Dao
interface MedicineDAO {

    @Insert
    fun insertAll(vararg medicines: Medicine)

    @Transaction
    @Query("SELECT m.* FROM Medicine m WHERE m.id = :id")
    fun findById(id: Long): MedicineWithEverything

    @Transaction
    @Query("SELECT m.* FROM Medicine m WHERE m.id > 0 AND m.name LIKE :name")
    fun findByName(name: String): List<MedicineWithReleaseFormAndProducer>

    @Transaction
    @Query("SELECT m.* FROM Medicine m WHERE m.id > 0 AND m.inn LIKE :inn")
    fun findByInn(inn: String): List<MedicineWithReleaseFormAndProducer>

    @Transaction
    @Query("SELECT m.* FROM Medicine m INNER JOIN " +
            "MedicineActiveSubstance mas ON m.id = mas.medicineId INNER JOIN " +
            "ActiveSubstance a ON mas.activeSubstanceId = a.id " +
            "WHERE m.id > 0 AND a.name LIKE :substanceName")
    fun findBySubstanceName(substanceName: String): List<MedicineWithReleaseFormAndProducer>

    @Transaction
    @Query("SELECT m.* FROM Medicine m INNER JOIN " +
            "CategoryMedicine cm ON m.id = cm.medicineId " +
            "WHERE m.id > 0 AND cm.categoryId = :categoryId")
    fun findByCategoryId(categoryId: Long): List<MedicineWithReleaseFormAndProducer>

    @Transaction
    @Query(" SELECT * FROM MedicineInfo")
    fun findWithNotes(): LiveData<List<MedicineWithNotes>>
}