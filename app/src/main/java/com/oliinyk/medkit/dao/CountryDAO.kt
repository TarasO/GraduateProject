package com.oliinyk.medkit.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.oliinyk.medkit.model.Country

@Dao
interface CountryDAO {

    @Insert
    fun insertAll(vararg countries: Country)

    @Query("SELECT COUNT(*) FROM Country")
    fun count(): Int
}