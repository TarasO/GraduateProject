package com.oliinyk.medkit.dao

import androidx.room.*
import com.oliinyk.medkit.model.MedicineState
import com.oliinyk.medkit.model.SetMedicine
import java.time.LocalDate

@Dao
interface SetMedicineDAO {

    @Insert
    fun insertAll(vararg setMedicines: SetMedicine)

    @Update
    fun update(setMedicine: SetMedicine)

    @Query("UPDATE SetMedicine SET state = :state " +
            "WHERE setId = :setId AND medicineId = :medicineId")
    fun updateMedicineState(setId: Long, medicineId: Long, state: MedicineState)

    @Query("UPDATE SetMedicine SET expirationDate = :expirationDate " +
            "WHERE setId = :setId AND medicineId = :medicineId")
    fun updateMedicineExpirationDate(setId: Long, medicineId: Long, expirationDate: LocalDate)

    @Query("SELECT sm.* FROM SetMedicine sm WHERE sm.expirationDate IS NOT NULL")
    fun findAll(): List<SetMedicine>

    @Query("SELECT sm.medicineId FROM SetMedicine sm WHERE sm.setId = :setId")
    fun findBySetId(setId: Long): List<Long>

    @Query("SELECT sm.* FROM SetMedicine sm " +
            "WHERE sm.setId = :setId AND medicineId = :medicineId")
    fun findBySetIdAndMedicineId(setId: Long, medicineId: Long): SetMedicine

    @Query("SELECT EXISTS(SELECT sm.* FROM SetMedicine sm " +
            "WHERE sm.medicineId = :medicineId AND sm.setId = :setId)")
    fun existsByMedicineIdAndSetId(medicineId: Long, setId: Long): Boolean

    @Query("DELETE FROM SetMedicine WHERE setId = :setId AND medicineId = :medicineId")
    fun delete(setId: Long, medicineId: Long)
}