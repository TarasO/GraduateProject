package com.oliinyk.medkit.dao

import androidx.room.Dao
import androidx.room.Insert
import com.oliinyk.medkit.model.Producer

@Dao
interface ProducerDAO {

    @Insert
    fun insertAll(vararg producers: Producer)
}