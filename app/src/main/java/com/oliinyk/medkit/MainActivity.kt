package com.oliinyk.medkit

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.oliinyk.medkit.databinding.ActivityMainBinding
import com.oliinyk.medkit.model.MedicineState
import com.oliinyk.medkit.ui.search.SearchActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment_content_main) as NavHostFragment
        val navController = navHostFragment.navController
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_car, R.id.nav_home, R.id.nav_schedule, R.id.nav_notes, R.id.nav_map, R.id.nav_help
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        MedkitDatabase.buildDatabase(applicationContext)

        val viewModel =
                ViewModelProvider(this)[MainActivityViewModel::class.java]

        val inflatedView = LayoutInflater.from(this)
                                         .inflate(R.layout.layout_progress_bar, null)
        val progressDialog = AlertDialog.Builder(this)
                                        .setTitle("Підготовка додатку")
                                        .setView(inflatedView)
                                        .create()

        viewModel.connectionError.observe(this) {
            if(it) {
                Toast.makeText(applicationContext,
                               "Помилка з'єднання з сервером",
                                    Toast.LENGTH_SHORT)
                     .show()
                progressDialog.cancel()
            }
        }

        viewModel.isEmpty.observe(this) {
            if(it) {
                progressDialog.show()
            } else {
                progressDialog.cancel()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.search_toolbar_button -> {
                val intent = Intent(applicationContext, SearchActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onResume() {
        super.onResume()
        val setMedicineDao = MedkitDatabase.getDatabase(applicationContext).setMedicineDao()
        CoroutineScope(Dispatchers.IO).launch {
            val list = setMedicineDao.findAll().filter { x -> x.state != MedicineState.EMPTY }
            val now = LocalDate.now()
            list.forEach {
                if (now.isAfter(it.expirationDate)) {
                    setMedicineDao.updateMedicineState(
                        it.setId,
                        it.medicineId,
                        MedicineState.EXPIRED
                    )
                } else if (now.isAfter(it.expirationDate!!.minusDays(7))
                    && now.isBefore(it.expirationDate)
                ) {
                    setMedicineDao.updateMedicineState(
                        it.setId,
                        it.medicineId,
                        MedicineState.EXPIRING
                    )
                }
            }
        }
    }
}
