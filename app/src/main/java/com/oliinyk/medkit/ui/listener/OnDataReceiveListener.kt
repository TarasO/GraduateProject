package com.oliinyk.medkit.ui.listener

interface OnDataReceiveListener<T> {
    fun onReceive(data: T)
}