package com.oliinyk.medkit.ui.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oliinyk.medkit.R
import com.oliinyk.medkit.model.relationship.MedicineWithReleaseFormAndProducer
import com.oliinyk.medkit.ui.listener.OnItemClickListener
import com.oliinyk.medkit.ui.listener.OnItemSelectedListener

class ResultsRecyclerAdapter(private val context: Context,
                             private val dataSet: List<MedicineWithReleaseFormAndProducer>,
                             private val medicineIdInSet: List<Long>?,
                             private val onItemSelectedListener:
                                OnItemSelectedListener<MedicineWithReleaseFormAndProducer>):
    RecyclerView.Adapter<ResultsRecyclerAdapter.ViewHolder>(), OnItemClickListener {

    class ViewHolder(view: View,
                     private val onItemClickListener:
                     OnItemClickListener
                     ): RecyclerView.ViewHolder(view), View.OnClickListener {
        val itemName: TextView = view.findViewById(R.id.item_results_name)
        val itemReleaseForm: TextView = view.findViewById(R.id.item_results_release_form)
        val itemProducer: TextView = view.findViewById(R.id.item_results_producer)
        val checkmarkImage: ImageView = view.findViewById(R.id.checkmark_image)

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onItemClickListener.onItemClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search_medicine, parent, false)

        return ViewHolder(view, this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataSet[position]
        holder.itemName.text = context.getString(R.string.results_item_name,
                                                 item.medicine.name,
                                                 item.medicine.inn)
        holder.itemReleaseForm.text = context.getString(R.string.results_item_release_form,
                                                        item.releaseForm.name)
        holder.itemProducer.text = context.getString(R.string.results_item_producer,
                                                     item.producerWithCountry.producer.name,
                                                     item.producerWithCountry.country.name)
        medicineIdInSet?.let {
            holder.checkmarkImage.visibility = it.contains(item.medicine.id).compareTo(true)
        }
    }

    override fun getItemCount() = dataSet.size

    override fun onItemClick(position: Int) {
        onItemSelectedListener.onItemSelected(dataSet[position])
    }
}