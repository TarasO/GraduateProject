package com.oliinyk.medkit.ui.search

import java.io.Serializable

enum class MedicineDisplay: Serializable {
    SET, NOTE, VIEW
}