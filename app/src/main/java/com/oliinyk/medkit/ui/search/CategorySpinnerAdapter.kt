package com.oliinyk.medkit.ui.search

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.oliinyk.medkit.model.tuple.CategoryNameTuple

class CategorySpinnerAdapter(context: Context, textViewResourceId: Int,
                             private val categories: MutableList<CategoryNameTuple>
):
    ArrayAdapter<CategoryNameTuple>(context, textViewResourceId, categories) {

    init {
        if(categories.size == 0) {
            categories.add(CategoryNameTuple(-2, "Немає категорій"))
            categories.add(CategoryNameTuple(-2, "Немає категорій"))
        } else {
            categories.add(CategoryNameTuple(-1, "Оберіть категорію"))
        }
    }

    override fun getCount(): Int {
        return categories.size-1
    }

    override fun getItem(position: Int): CategoryNameTuple {
        return categories[position]
    }

    override fun getItemId(position: Int): Long {
        return categories[position].id
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getView(position, convertView, parent) as TextView
        if(position == count) {
            label.setTextColor(Color.GRAY)
        } else {
            label.setTextColor(Color.BLACK)
        }
        label.text = categories[position].name
        return label
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getDropDownView(position, convertView, parent) as TextView
        label.setTextColor(Color.BLACK)
        label.text = categories[position].name
        return label
    }
}