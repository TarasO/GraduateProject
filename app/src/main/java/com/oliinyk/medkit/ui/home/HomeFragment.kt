package com.oliinyk.medkit.ui.home

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputEditText
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.FragmentHomeBinding
import com.oliinyk.medkit.model.Medicine
import com.oliinyk.medkit.model.MedicineState.EMPTY
import com.oliinyk.medkit.model.MedkitSet
import com.oliinyk.medkit.model.SetType.HOME
import com.oliinyk.medkit.model.relationship.SetWithMedicines
import com.oliinyk.medkit.model.tuple.MedicineWithExpiration
import com.oliinyk.medkit.ui.listener.OnItemSelectedListener
import com.oliinyk.medkit.ui.search.MedicineDisplay
import com.oliinyk.medkit.ui.search.SearchActivity
import com.oliinyk.medkit.viewmodel.home.HomeViewModel
import java.time.DateTimeException
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class HomeFragment : Fragment(), OnItemSelectedListener<MedicineWithExpiration> {

    private var _binding: FragmentHomeBinding? = null

    private val binding get() = _binding!!

    private lateinit var viewModel: HomeViewModel

    private var searchRegister: ActivityResultLauncher<Intent>? = null
    private var medicine: MutableLiveData<Medicine?> = MutableLiveData(null)
    private val dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel =
            ViewModelProvider(this)[HomeViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val spinner = binding.homeSetSpinner

        val renameSet = binding.homeSetEdit
        renameSet.setOnClickListener {
            renameSetDialog((spinner.selectedItem as SetWithMedicines).medkitSet)
        }

        val deleteSet = binding.homeSetDelete
        deleteSet.setOnClickListener {
            deleteSetDialog((spinner.selectedItem as SetWithMedicines).medkitSet)
        }

        val setEfab = binding.homeEfabSet
        setEfab.setOnClickListener {
            addSetDialog()
        }

        val medicineEfab = binding.homeEfabMedicine
        medicineEfab.setOnClickListener {
            val set = (spinner.selectedItem as SetWithMedicines).medkitSet
            addMedicineDialog(set)
        }

        val emptyListLabel = binding.emptyListLabel

        val recyclerView = binding.homeRecycler
        recyclerView.addItemDecoration(
            DividerItemDecoration(recyclerView.context,
            DividerItemDecoration.VERTICAL)
        )
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        val fragment = this
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val isSet = id != -1L
                medicineEfab.fabOptionEnabled = isSet
                renameSet.isEnabled = isSet
                deleteSet.isEnabled = isSet

                if(isSet){
                    val enabledColor = ContextCompat
                        .getColor(requireContext(), R.color.efab_label_background)
                    medicineEfab.label.background.colorFilter = BlendModeColorFilterCompat.
                        createBlendModeColorFilterCompat(enabledColor, BlendModeCompat.SRC_ATOP)

                    val setWithMedicines = spinner.selectedItem as SetWithMedicines
                    if(setWithMedicines.medicines.isNotEmpty()) {
                        val adapter = SetMedicineRecyclerAdapter(
                            requireContext(),
                            setWithMedicines.medicines,
                            fragment
                        )
                        recyclerView.adapter = adapter
                        emptyListLabel.visibility = View.GONE
                    } else {
                        recyclerView.adapter = null
                        emptyListLabel.visibility = View.VISIBLE
                    }
                    renameSet.drawable.setTintList(null)
                    deleteSet.drawable.setTintList(null)
                } else {
                    renameSet.drawable
                        .setTint(requireContext()
                            .getColor(com.google.android.libraries.places.R.color.quantum_grey700))
                    deleteSet.drawable
                        .setTint(requireContext()
                            .getColor(com.google.android.libraries.places.R.color.quantum_grey700))
                    recyclerView.adapter = null
                    emptyListLabel.visibility = View.GONE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        viewModel.sets.observe(viewLifecycleOwner) {
            val id = spinner.getItemIdAtPosition(spinner.lastVisiblePosition)

            spinner.isEnabled = it.isNotEmpty()
            val adapter = SetSpinnerAdapter(requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                it.toMutableList())
            spinner.adapter = adapter

            val item = it.firstOrNull { x -> x.medkitSet.id == id }
            if(item != null) {
                spinner.setSelection(it.indexOf(item))
            } else {
                spinner.setSelection(adapter.count)
            }
        }

        searchRegister = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val data = it.data
                medicine.value = data!!.getSerializableExtra("medicine") as Medicine
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemSelected(item: MedicineWithExpiration) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(item.name)
               .setItems(requireContext().resources.getStringArray(R.array.set_medicine_options)
               ) { dialogInterface, i ->
                   dialogInterface.dismiss()
                   when (i) {
                       0 -> {
                           val bundle = Bundle()
                           bundle.putSerializable("medicineId", item.id)
                           bundle.putSerializable("displayType", MedicineDisplay.VIEW)
                           findNavController().navigate(R.id.action_view_medicine_fragment, bundle)
                       }
                       1 -> {
                           val restockBuilder = AlertDialog.Builder(requireContext())

                           val inflatedView = LayoutInflater.from(requireContext())
                               .inflate(R.layout.single_text_input_dialog,
                                   view as ViewGroup,
                                   false)
                           val textInput = inflatedView.findViewById<TextInputEditText>(
                                               R.id.single_text_input
                                           ) as EditText

                           textInput.isCursorVisible = false
                           textInput.isFocusable = false
                           textInput.hint = requireContext()
                               .getString(R.string.add_medicine_expiration_edit)

                           var expirationDate: LocalDate? = null
                           val calendar = Calendar.getInstance()

                           textInput.setOnClickListener {
                               DatePickerDialog(requireContext(),
                                   { _, year, month, day ->
                                       try {
                                           expirationDate = LocalDate.of(year, month+1, day)
                                           textInput.setText(expirationDate!!.format(dateFormat))
                                       } catch (e: DateTimeException) {
                                           Toast.makeText(requireContext(),
                                               "Невірна дата", Toast.LENGTH_SHORT).show()
                                       }
                                   },
                                   calendar.get(Calendar.YEAR),
                                   calendar.get(Calendar.MONTH),
                                   calendar.get(Calendar.DAY_OF_MONTH)
                               ).show()
                           }

                           restockBuilder.setTitle("Поновити препарат")
                               .setView(inflatedView)
                               .setPositiveButton("Поновити") { dialog, _ ->
                                   if(textInput.text.isNotBlank()) {
                                       dialog.dismiss()
                                       viewModel.updateMedicineExpirationDate(
                                           item.setId,
                                           item.id,
                                           expirationDate!!
                                       )
                                   }
                               }
                               .setNegativeButton("Скасувати") { dialog, _ ->
                                   dialog.cancel()
                               }.show()
                       }
                       2 -> {
                           viewModel.updateMedicineState(item.setId, item.id, EMPTY)
                       }
                       3 -> {
                           val deleteBuilder = AlertDialog.Builder(requireContext())

                           deleteBuilder.setTitle("Видалити препарат з набору?")
                               .setPositiveButton("Видалити") { dialog, _ ->
                                   dialog.dismiss()
                                   viewModel.removeMedicineFromSet(item.setId, item.id)
                               }
                               .setNegativeButton("Скасувати") { dialog, _ -> dialog.cancel() }.
                               show()
                       }
                   }
               }.show()
    }

    private fun deleteSetDialog(set: MedkitSet) {
        val builder = AlertDialog.Builder(requireContext())

        builder.setTitle("Видалити набір?")
            .setPositiveButton("Видалити") { dialog, _ ->
                dialog.dismiss()
                viewModel.deleteSet(set)
            }
            .setNegativeButton("Скасувати") { dialog, _ -> dialog.cancel() }.
            show()
    }

    private fun renameSetDialog(set: MedkitSet) {
        val builder = AlertDialog.Builder(requireContext())

        val inflatedView = LayoutInflater.from(requireContext())
            .inflate(R.layout.single_text_input_dialog,
                view as ViewGroup,
                false)

        val input = inflatedView.findViewById<TextInputEditText>(R.id.single_text_input) as EditText
        builder.setTitle("Назва набору")
            .setView(inflatedView)
            .setPositiveButton("Перейменувати") { dialog, _ ->
                dialog.dismiss()

                viewModel.updateSet(MedkitSet(set.id, input.text.toString(), set.setType))
            }
            .setNegativeButton("Скасувати") { dialog, _ -> dialog.cancel() }.
            show()
    }

    private fun addSetDialog() {
        val builder = AlertDialog.Builder(requireContext())

        val inflatedView = LayoutInflater.from(requireContext())
                                         .inflate(R.layout.single_text_input_dialog,
                                                  view as ViewGroup,
                                                  false)

        val input = inflatedView.findViewById<TextInputEditText>(R.id.single_text_input) as EditText
        input.hint = "Назва набору"
        builder.setTitle("Додати набір")
               .setView(inflatedView)
               .setPositiveButton("Додати") { dialog, _ ->
                   if(input.text.isNotBlank()) {
                       dialog.dismiss()
                       viewModel.addSet(MedkitSet(input.text.toString(), HOME))
                   } else {
                       Toast.makeText(requireContext(),
                                      "Назва не повинне бути порожньою",
                                      Toast.LENGTH_SHORT).show()
                   }
               }
               .setNegativeButton("Скасувати") { dialog, _ -> dialog.cancel() }.
               show()
    }

    private fun addMedicineDialog(set: MedkitSet) {
        val builder = AlertDialog.Builder(requireContext())

        val inflatedView = LayoutInflater.from(requireContext())
            .inflate(R.layout.dialog_add_medicine,
                view as ViewGroup,
                false)

        val medicineInput = inflatedView.findViewById<TextInputEditText>(R.id.medicine_name_input) as EditText
        medicineInput.setOnClickListener {
            val intent = Intent(requireContext(), SearchActivity::class.java)
            intent.putExtra("setId", set.id)
            searchRegister!!.launch(intent)
        }

        medicine.observe(viewLifecycleOwner) {
            it?.let {
                medicineInput.setText(it.name)
            }
        }

        val expirationDateInput =
            inflatedView.findViewById<TextInputEditText>(R.id.medicine_expiration_input) as EditText

        var expirationDate: LocalDate? = null
        val calendar = Calendar.getInstance()

        expirationDateInput.setOnClickListener {
            DatePickerDialog(requireContext(),
                { _, year, month, day ->
                    try {
                        expirationDate = LocalDate.of(year, month+1, day)
                        expirationDateInput.setText(expirationDate!!.format(dateFormat))
                    } catch (e: DateTimeException) {
                        Toast.makeText(requireContext(),
                            "Невірна дата", Toast.LENGTH_SHORT).show()
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        builder.setTitle("Додати препарат")
            .setView(inflatedView)
            .setPositiveButton("Додати") { dialog, _ ->
                if(medicineInput.text.isNotBlank() && expirationDateInput.text.isNotBlank()) {
                    dialog.dismiss()
                    viewModel.addMedicineToSet(set, medicine.value!!, expirationDate!!)
                    medicine.value = null
                }
            }
            .setNegativeButton("Скасувати") { dialog, _ ->
                dialog.cancel()
                medicine.value = null
            }
            .setOnCancelListener { medicine.value = null }.show()
    }
}