package com.oliinyk.medkit.ui.notes

import android.animation.ObjectAnimator
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.oliinyk.medkit.R
import com.oliinyk.medkit.model.Note
import com.oliinyk.medkit.model.relationship.MedicineWithNotes
import com.oliinyk.medkit.model.tuple.MedicineInfo
import com.oliinyk.medkit.ui.listener.OnItemLongClickListener
import com.oliinyk.medkit.ui.listener.OnItemSelectedListener
import com.oliinyk.medkit.ui.listener.OnNestedItemSelectedListener

class MedicineNoteRecyclerAdapter(private val context: Context,
                                  private val dataSet: List<MedicineWithNotes>,
                                  private val onItemSelectedListener: OnItemSelectedListener<MedicineInfo>,
                                  private val onNestedItemSelectedListener: OnNestedItemSelectedListener<Note>) :
    RecyclerView.Adapter<MedicineNoteRecyclerAdapter.ViewHolder>(), OnItemLongClickListener {

    class ViewHolder(view: View, private val onItemLongClickListener: OnItemLongClickListener):
        RecyclerView.ViewHolder(view), View.OnClickListener,
        View.OnLongClickListener {

        val itemName: TextView = view.findViewById(R.id.medicine_note_name)
        val itemReleaseForm: TextView = view.findViewById(R.id.medicine_note_release_form)
        val itemProducer: TextView = view.findViewById(R.id.medicine_note_producer)
        val itemNoteCount: TextView = view.findViewById(R.id.medicine_note_count)
        val arrowIcon: ImageView = view.findViewById(R.id.medicine_note_arrow_image)
        val itemNoteRecycler: RecyclerView = view.findViewById(R.id.medicine_note_recycler)

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            if(itemNoteRecycler.isVisible) {
                itemNoteRecycler.visibility = View.GONE
                ObjectAnimator.ofFloat(arrowIcon, View.ROTATION, 90f, 0f)
                    .setDuration(300).start()
            } else {
                itemNoteRecycler.visibility = View.VISIBLE
                ObjectAnimator.ofFloat(arrowIcon, View.ROTATION, 0f, 90f)
                    .setDuration(300).start()
            }
        }

        override fun onLongClick(v: View?): Boolean {
            return onItemLongClickListener.onItemLongClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_medicine_note, parent, false)

        return ViewHolder(view, this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataSet[position]
        holder.itemName.text = context.getString(R.string.medicine_display_name, item.medicine.name)
        holder.itemReleaseForm.text = context
            .getString(R.string.results_item_release_form, item.medicine.releaseFormName)
        holder.itemProducer.text = context
            .getString(R.string.set_medicine_producer, item.medicine.producerName)
        holder.itemNoteCount.text = context.getString(R.string.medicine_note_count, item.notes.size)

        val recycler = holder.itemNoteRecycler
        recycler.addItemDecoration(
            DividerItemDecoration(recycler.context,
                DividerItemDecoration.VERTICAL)
        )
        recycler.layoutManager = LinearLayoutManager(context)

        recycler.adapter = NoteRecyclerAdapter(context,
                                              dataSet[position].notes,
                                              onNestedItemSelectedListener)
    }

    override fun getItemCount(): Int = dataSet.size

    override fun onItemLongClick(position: Int): Boolean {
        onItemSelectedListener.onItemSelected(dataSet[position].medicine)
        return true
    }
}
