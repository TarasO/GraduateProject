package com.oliinyk.medkit.ui.search

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.ActivitySearchBinding
import com.oliinyk.medkit.model.Medicine
import com.oliinyk.medkit.ui.listener.OnDataReceiveListener

class SearchActivity : AppCompatActivity(), OnDataReceiveListener<Medicine> {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivitySearchBinding
    private lateinit var navController: NavController
    var currentSetId: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarSearch.searchToolbar)

        appBarConfiguration = AppBarConfiguration.Builder()
                                .setFallbackOnNavigateUpListener { onNavigateUp() }
                                .build()

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment_content_search) as NavHostFragment
        navController = navHostFragment.navController

        setupActionBarWithNavController(navController, appBarConfiguration)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        intent.extras?.let {
            if(it.containsKey("setId")) {
                currentSetId = intent.extras!!.getLong("setId")
            }
        }

        navController.backQueue.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                return if(navController.backQueue.size == 0) {
                    onBackPressed()
                    true
                } else {
                    navController.popBackStack()
                    false
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onReceive(data: Medicine) {
        val intent = Intent()
        intent.putExtra("medicine", data)
        setResult(RESULT_OK, intent)
        finish()
    }
}