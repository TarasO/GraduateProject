package com.oliinyk.medkit.ui.notes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oliinyk.medkit.R
import com.oliinyk.medkit.model.Note
import com.oliinyk.medkit.ui.listener.OnItemClickListener
import com.oliinyk.medkit.ui.listener.OnNestedItemSelectedListener

class NoteRecyclerAdapter(private val context: Context,
                          private val dataSet: List<Note>,
                          private val onNestedItemSelectedListener: OnNestedItemSelectedListener<Note>) :
    RecyclerView.Adapter<NoteRecyclerAdapter.ViewHolder>(), OnItemClickListener {

    class ViewHolder(view: View, private val onItemClickListener: OnItemClickListener):
        RecyclerView.ViewHolder(view), View.OnClickListener {

        val itemPerson: TextView = view.findViewById(R.id.note_person)
        val itemDescription: TextView = view.findViewById(R.id.note_description)

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            onItemClickListener.onItemClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_note, parent, false)

        return ViewHolder(view, this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataSet[position]
        item.personName?.let {
            holder.itemPerson.text = context.getString(R.string.note_person, item.personName)
            holder.itemPerson.visibility = View.VISIBLE
        }

        holder.itemDescription.text = item.description
    }

    override fun getItemCount(): Int = dataSet.size

    override fun onItemClick(position: Int) {
        onNestedItemSelectedListener.onNestedItemSelected(dataSet[position])
    }
}
