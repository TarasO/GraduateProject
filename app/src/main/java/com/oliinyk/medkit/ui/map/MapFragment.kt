package com.oliinyk.medkit.ui.map

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.FragmentMapBinding
import com.oliinyk.medkit.viewmodel.map.MapViewModel

class MapFragment : Fragment() {

    private var _binding: FragmentMapBinding? = null

    private lateinit var pharmacyNav: Button
    private lateinit var findPharmacy: Button

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val viewModel =
            ViewModelProvider(this)[MapViewModel::class.java]

        _binding = FragmentMapBinding.inflate(inflater, container, false)
        val root: View = binding.root

        pharmacyNav = root.findViewById(R.id.pharmacyNav)
        findPharmacy = root.findViewById(R.id.findPharmacy)

        pharmacyNav.setOnClickListener {
            val intent = Intent(context, ClosestPharmacyRouteActivity::class.java)
            startActivity(intent)
        }

        findPharmacy.setOnClickListener {
            val intent = Intent(context, FindPharmacyActivity::class.java)
            startActivity(intent)
        }

        val granted = ContextCompat.checkSelfPermission(this.requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this.requireContext(), Manifest.permission.RECORD_AUDIO) ==
                PackageManager.PERMISSION_GRANTED
        if(!granted) {
            val requestMultiplePermissions = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {

            }

            requestMultiplePermissions.launch(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO))
        }
//        val textView: TextView = binding.textMap
//        mapViewModel.text.observe(viewLifecycleOwner) {
//            textView.text = it
//        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}