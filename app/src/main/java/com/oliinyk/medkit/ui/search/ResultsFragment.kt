package com.oliinyk.medkit.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.FragmentResultsBinding
import com.oliinyk.medkit.model.relationship.MedicineWithReleaseFormAndProducer
import com.oliinyk.medkit.model.tuple.CategoryNameTuple
import com.oliinyk.medkit.ui.listener.OnItemSelectedListener
import com.oliinyk.medkit.viewmodel.search.ResultsViewModel

class ResultsFragment : Fragment(), OnItemSelectedListener<MedicineWithReleaseFormAndProducer> {

    private var _binding: FragmentResultsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: ResultsViewModel
    private lateinit var query: String
    private var currentSetId: Long = -1L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this)[ResultsViewModel::class.java]

        currentSetId = (requireActivity() as SearchActivity).currentSetId

        val args = requireArguments()
        when(args["search"]) {
            "name" -> {
                query = args.getString("value")!!
                viewModel.findByName(query)
            }
            "inn" -> {
                query = args.getString("value")!!
                viewModel.findByInn(query)
            }
            "substance" -> {
                query = args.getString("value")!!
                viewModel.findBySubstanceName(query)
            }
            "category" -> {
                val category = args.getSerializable("value") as CategoryNameTuple
                query = category.name
                viewModel.findByCategoryId(category.id)
            }
        }

        if(currentSetId != -1L) {
            viewModel.findMedicinesIdInSet(currentSetId)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val viewModel = ViewModelProvider(this)[ResultsViewModel::class.java]

        _binding = FragmentResultsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val notFoundLabel = binding.notFoundLabel


        notFoundLabel.text = getString(R.string.results_not_found, query)

        val recyclerView = binding.resultsRecyclerView
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context,
                                                             DividerItemDecoration.VERTICAL))
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        viewModel.medicines.observe(viewLifecycleOwner) {
            if(it.isNotEmpty()) {
                val adapter = ResultsRecyclerAdapter(
                    requireContext(),
                    it,
                    viewModel.medicinesInSet,
                    this
                )
                recyclerView.adapter = adapter
                notFoundLabel.visibility = View.GONE
            } else {
                notFoundLabel.visibility = View.VISIBLE
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemSelected(item: MedicineWithReleaseFormAndProducer) {
        val navController = findNavController()
        val bundle = Bundle()
        bundle.putSerializable("medicineId", item.medicine.id)
        val display = if(currentSetId > 0) {
            MedicineDisplay.SET
        } else if(currentSetId == 0L) {
            MedicineDisplay.NOTE
        } else {
            MedicineDisplay.VIEW
        }
        bundle.putSerializable("displayType", display)
        navController.navigate(R.id.action_to_medicine_display_fragment, bundle)
    }
}