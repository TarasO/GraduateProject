package com.oliinyk.medkit.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.FragmentMedicineDisplayBinding
import com.oliinyk.medkit.model.Medicine
import com.oliinyk.medkit.ui.listener.OnDataReceiveListener
import com.oliinyk.medkit.ui.search.MedicineDisplay.*
import com.oliinyk.medkit.viewmodel.search.MedicineDisplayViewModel

class MedicineDisplayFragment : Fragment() {

    private var _binding: FragmentMedicineDisplayBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: MedicineDisplayViewModel
    private lateinit var displayType: MedicineDisplay

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this)[MedicineDisplayViewModel::class.java]

        displayType = requireArguments().getSerializable("displayType") as MedicineDisplay

        val medicineId = requireArguments().getLong("medicineId")
        viewModel.findMedicineById(medicineId)

        when(displayType) {
            SET -> {
                val currentSetId = (requireActivity() as SearchActivity).currentSetId
                viewModel.existsByMedicineIdAndSetId(medicineId, currentSetId)
            }
            NOTE -> {
                viewModel.hasNotesByMedicineId(medicineId)
            }
            VIEW -> {
                setHasOptionsMenu(false)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMedicineDisplayBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val display: Boolean = when(displayType) {
            SET -> {
                !viewModel.isInSet
            }
            NOTE -> {
                !viewModel.hasNotes
            }
            VIEW -> {
                false
            }
        }
        setHasOptionsMenu(display)

        val nameTextView = binding.medicineDisplayName
        val innTextView = binding.medicineDisplayInn
        val releaseFormTextView = binding.medicineDisplayReleaseForm
        val registrationNumberTextView = binding.medicineDisplayRegistrationNumber
        val producerTextView = binding.medicineDisplayProducer
        val substanceTextView = binding.medicineDisplaySubstance
        val categoryTextView = binding.medicineDisplayCategory
        val expirationPeriodTextView = binding.medicineDisplayExpirationPeriod
        val byRecipeTextView = binding.medicineDisplayByRecipe
        val viewManualButton = binding.viewManualButton

        viewModel.medicineWithEverything.observe(viewLifecycleOwner) {
            val medicine = it.medicine
            val releaseForm = it.releaseForm
            val producer = it.producerWithCountry.producer
            val country = it.producerWithCountry.country
            val substances = it.substances
            val categories = it.categories

            nameTextView.text = getString(R.string.medicine_display_name, medicine.name)
            innTextView.text = getString(R.string.medicine_display_inn, medicine.inn)
            releaseFormTextView.text =
                getString(R.string.results_item_release_form, releaseForm.name)
            registrationNumberTextView.text = getString(
                R.string.medicine_display_registration_number,
                medicine.registrationNumber
            )
            producerTextView.text =
                getString(R.string.results_item_producer, producer.name, country.name)

            if (substances.isNotEmpty()) {
                val substancesString =
                    substances.map { s -> s.name }.reduce { acc, s -> "$acc, $s" }
                substanceTextView.text = substancesString
            }

            if (categories.isNotEmpty()) {
                val categoriesString =
                    categories.map { c -> c.name }.reduce { acc, c -> "$acc, $c" }
                categoryTextView.text = categoriesString
            }

            if (medicine.expirationPeriod != null) {
                expirationPeriodTextView.text = requireContext()
                    .getString(
                        R.string.medicine_display_expiration_period,
                        medicine.expirationPeriod
                    )
            } else {
                expirationPeriodTextView.visibility = View.GONE
            }

            val byRecipe = if (medicine.byRecipe) {
                "Так"
            } else {
                "Ні"
            }
            byRecipeTextView.text = getString(R.string.medicine_display_by_recipe, byRecipe)

            viewManualButton.isEnabled =
                medicine.manualUrl != null && medicine.manualUrl.isNotBlank()

            viewManualButton.setOnClickListener {
                val intent = Intent(requireContext(), WebViewActivity::class.java)
                intent.putExtra("url", medicine.manualUrl)
                startActivity(intent)
            }
        }

        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.add_from_display_button -> {
                (requireActivity() as OnDataReceiveListener<Medicine>)
                    .onReceive(viewModel.medicineWithEverything.value!!.medicine)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}