package com.oliinyk.medkit.ui.search

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.ActivityWebviewBinding


class WebViewActivity : AppCompatActivity() {

    private var _binding: ActivityWebviewBinding? = null

    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        _binding = ActivityWebviewBinding.inflate(layoutInflater)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val url = intent.getStringExtra("url")

        val webView = binding.webView
        val settings = webView.settings

        settings.domStorageEnabled = true
        settings.javaScriptEnabled = true
        webView.loadUrl(url!!)

        binding.root.removeAllViews()
        setContentView(webView)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}