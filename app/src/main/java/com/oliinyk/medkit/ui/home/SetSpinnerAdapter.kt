package com.oliinyk.medkit.ui.home

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.oliinyk.medkit.model.MedkitSet
import com.oliinyk.medkit.model.SetType
import com.oliinyk.medkit.model.SetType.STUB
import com.oliinyk.medkit.model.relationship.SetWithMedicines


class SetSpinnerAdapter(context: Context, textViewResourceId: Int,
                        private val sets: MutableList<SetWithMedicines>
): ArrayAdapter<SetWithMedicines>(context, textViewResourceId, sets) {

    init {
        when (sets.size) {
            0 -> {
                sets.add(SetWithMedicines(MedkitSet("Немає наборів", STUB), emptyList()))
                sets.add(SetWithMedicines(MedkitSet("Немає наборів", STUB), emptyList()))
            }
            else -> {
                sets.add(SetWithMedicines(MedkitSet("Оберіть набір", STUB), emptyList()))
            }
        }
    }

    override fun getCount(): Int {
        return sets.size-1
    }

    override fun getItem(position: Int): SetWithMedicines {
        return sets[position]
    }

    override fun getItemId(position: Int): Long {
        var id = -1L
        if(position != count) {
            id = sets[position].medkitSet.id
        }
        return id
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getView(position, convertView, parent) as TextView
        if(position == count) {
            label.setTextColor(Color.GRAY)
        } else {
            label.setTextColor(Color.BLACK)
        }
        label.text = sets[position].medkitSet.name
        return label
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getDropDownView(position, convertView, parent) as TextView
        label.setTextColor(Color.BLACK)
        label.text = sets[position].medkitSet.name
        return label
    }
}