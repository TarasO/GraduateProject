package com.oliinyk.medkit.ui.listener

interface OnItemSelectedListener<T> {
    fun onItemSelected(item: T)
}