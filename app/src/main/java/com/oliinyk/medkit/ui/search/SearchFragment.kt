package com.oliinyk.medkit.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.FragmentSearchBinding
import com.oliinyk.medkit.model.tuple.CategoryNameTuple
import com.oliinyk.medkit.viewmodel.search.SearchViewModel

class SearchFragment : Fragment() {

    private var _binding: FragmentSearchBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val viewModel = ViewModelProvider(this)[SearchViewModel::class.java]

        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val radioGroup = binding.searchRadioGroup

        val nameRadio = binding.nameRadio
        val nameInput = binding.nameSearchInput

        val innRadio = binding.innRadio
        val innInput = binding.innSearchInput

        val substanceRadio = binding.substanceRadio
        val substanceInput = binding.substanceSearchInput

        val categoryRadio = binding.categoryRadio
        val categorySpinner = binding.categorySearchSpinner

        val searchButton = binding.searchButton

        viewModel.categories.observe(viewLifecycleOwner) {
            categorySpinner.isEnabled = it.isNotEmpty()
            val adapter = CategorySpinnerAdapter(requireContext(),
                                                 android.R.layout.simple_spinner_dropdown_item,
                                                 it.toMutableList())
            categorySpinner.adapter = adapter
            categorySpinner.setSelection(adapter.count)
        }

        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            when(checkedId) {
                nameRadio.id -> {
                    searchButton.isEnabled = nameInput.text.isNotBlank()
                }
                innRadio.id -> {
                    searchButton.isEnabled = innInput.text.isNotBlank()
                }
                substanceRadio.id -> {
                    searchButton.isEnabled = substanceInput.text.isNotBlank()
                }
                categoryRadio.id -> {
                    searchButton.isEnabled = categorySpinner.selectedItemId >= 0
                }
            }
            nameInput.isEnabled = nameRadio.id == checkedId
            innInput.isEnabled = innRadio.id == checkedId
            substanceInput.isEnabled = substanceRadio.id == checkedId
            categorySpinner.isEnabled = (categorySpinner.selectedItemId != -2L) &&
                    (categoryRadio.id == checkedId)
        }

        nameInput.doAfterTextChanged { searchButton.isEnabled = it!!.isNotBlank() }
        innInput.doAfterTextChanged { searchButton.isEnabled = it!!.isNotBlank() }
        substanceInput.doAfterTextChanged { searchButton.isEnabled = it!!.isNotBlank() }
        categorySpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, id: Long) {
                searchButton.isEnabled = id >= 0
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }

        val navController = findNavController()

        searchButton.setOnClickListener {
            val bundle = Bundle()
            when(radioGroup.checkedRadioButtonId) {
                nameRadio.id -> {
                    bundle.putString("search", "name")
                    bundle.putString("value", nameInput.text.toString())
                }
                innRadio.id -> {
                    bundle.putString("search", "inn")
                    bundle.putString("value", innInput.text.toString())
                }
                substanceRadio.id -> {
                    bundle.putString("search", "substance")
                    bundle.putString("value", substanceInput.text.toString())
                }
                categoryRadio.id -> {
                    bundle.putString("search", "category")
                    bundle.putSerializable("value", categorySpinner.selectedItem as CategoryNameTuple)
                }
            }
            navController.navigate(R.id.action_to_results_fragment, bundle)
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}