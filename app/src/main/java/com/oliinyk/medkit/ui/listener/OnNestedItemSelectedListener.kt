package com.oliinyk.medkit.ui.listener

interface OnNestedItemSelectedListener<T> {
    fun onNestedItemSelected(item: T)
}