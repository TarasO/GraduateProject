package com.oliinyk.medkit.ui.notes

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.FragmentNotesBinding
import com.oliinyk.medkit.model.Medicine
import com.oliinyk.medkit.model.Note
import com.oliinyk.medkit.model.tuple.MedicineInfo
import com.oliinyk.medkit.ui.listener.OnItemSelectedListener
import com.oliinyk.medkit.ui.listener.OnNestedItemSelectedListener
import com.oliinyk.medkit.ui.search.MedicineDisplay
import com.oliinyk.medkit.ui.search.SearchActivity
import com.oliinyk.medkit.util.hideKeyboard
import com.oliinyk.medkit.viewmodel.notes.NotesViewModel

class NotesFragment : Fragment(), OnItemSelectedListener<MedicineInfo>,
    OnNestedItemSelectedListener<Note> {

    private var _binding: FragmentNotesBinding? = null

    private val binding get() = _binding!!

    private lateinit var viewModel: NotesViewModel

    private var searchRegister: ActivityResultLauncher<Intent>? = null
    private var medicine: MutableLiveData<Medicine?> = MutableLiveData(null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this)[NotesViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val emptyLabel = binding.textNotes

        val recycler = binding.notesMedicineRecycler
        recycler.addItemDecoration(
            DividerItemDecoration(recycler.context,
                DividerItemDecoration.VERTICAL)
        )
        recycler.layoutManager = LinearLayoutManager(requireContext())

        viewModel.medicines.observe(viewLifecycleOwner) {
            if(it.isNotEmpty()) {
                recycler.adapter = MedicineNoteRecyclerAdapter(
                                       requireContext(),
                                       it,
                                       this,
                                       this
                                   )
                emptyLabel.visibility = View.GONE
            } else {
                recycler.adapter = null
                emptyLabel.visibility = View.VISIBLE
            }
        }

        searchRegister = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val data = it.data
                medicine.value = data!!.getSerializableExtra("medicine") as Medicine
            }
        }

        val fab = binding.notesFab
        fab.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())

            val inflatedView = LayoutInflater.from(requireContext())
                .inflate(R.layout.dialog_note,
                    view as ViewGroup,
                    false)

            val medicineInput = inflatedView.findViewById<TextInputEditText>(R.id.medicine_note_input) as EditText
            medicineInput.setOnClickListener {
                val intent = Intent(requireContext(), SearchActivity::class.java)
                intent.putExtra("setId", 0)
                searchRegister!!.launch(intent)
            }

            medicine.observe(viewLifecycleOwner) {
                it?.let {
                    medicineInput.setText(it.name)
                }
            }

            val personNameInput =
                inflatedView.findViewById<TextInputEditText>(R.id.note_person_input) as EditText

            val descriptionInput =
                inflatedView.findViewById<TextInputEditText>(R.id.note_description_input) as EditText

            builder.setTitle("Додати препарат")
                .setView(inflatedView)
                .setPositiveButton("Додати") { dialog, _ ->
                    if(medicineInput.text.isNotBlank() && descriptionInput.text.isNotBlank()) {
                        dialog.dismiss()
                        hideKeyboard()
                        val name = if(personNameInput.text.isBlank()) {
                            null
                        } else {
                            personNameInput.text.toString()
                        }
                        viewModel.addNote(
                            Note(
                                    name,
                                    descriptionInput.text.toString(),
                                    medicine.value!!.id
                                )
                        )
                        medicine.value = null
                    }
                }
                .setNegativeButton("Скасувати") { dialog, _ ->
                    dialog.cancel()
                    medicine.value = null
                }
                .setOnCancelListener { medicine.value = null }
                .show()
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemSelected(item: MedicineInfo) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(item.name)
            .setItems(requireContext().resources.getStringArray(R.array.medicine_note_options)
            ) { dialogInterface, i ->
                dialogInterface.dismiss()
                when (i) {
                    0 -> {
                        val bundle = Bundle()
                        bundle.putSerializable("medicineId", item.id)
                        bundle.putSerializable("displayType", MedicineDisplay.VIEW)
                        findNavController().navigate(R.id.action_view_medicine_fragment, bundle)
                    }
                    1 -> {
                        val noteBuilder = AlertDialog.Builder(requireContext())

                        val inflatedView = LayoutInflater.from(requireContext())
                            .inflate(R.layout.dialog_note,
                                view as ViewGroup,
                                false)

                        val medicineInputLayout = inflatedView
                            .findViewById<TextInputLayout>(R.id.medicine_note_input_layout)
                        medicineInputLayout.visibility = View.GONE

                        val personNameInput =
                            inflatedView.findViewById<TextInputEditText>(R.id.note_person_input) as EditText

                        val descriptionInput =
                            inflatedView.findViewById<TextInputEditText>(R.id.note_description_input) as EditText

                        noteBuilder.setTitle("Додати нотаток")
                            .setView(inflatedView)
                            .setPositiveButton("Додати") { dialog, _ ->
                                if(descriptionInput.text.isNotBlank()) {
                                    dialog.dismiss()
                                    hideKeyboard()
                                    val name = if(personNameInput.text.isBlank()) {
                                        null
                                    } else {
                                        personNameInput.text.toString()
                                    }
                                    viewModel.addNote(
                                        Note(
                                            name,
                                            descriptionInput.text.toString(),
                                            item.id
                                        )
                                    )
                                }
                            }
                            .setNegativeButton("Скасувати") { dialog, _ ->
                                dialog.cancel()
                            }
                            .show()
                    }
                    2 -> {
                        val deleteBuilder = AlertDialog.Builder(requireContext())

                        deleteBuilder.setTitle("Видалити всі нотатки?")
                            .setPositiveButton("Видалити") { dialog, _ ->
                                dialog.dismiss()
                                viewModel.deleteNotesByMedicineId(item.id)
                            }
                            .setNegativeButton("Скасувати") { dialog, _ -> dialog.cancel() }.
                            show()
                    }
                }
            }.show()
    }

    override fun onNestedItemSelected(item: Note) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Нотаток")
            .setItems(requireContext().resources.getStringArray(R.array.note_options)
            ) { dialogInterface, i ->
                dialogInterface.dismiss()
                when (i) {
                    0 -> {
                        val noteBuilder = AlertDialog.Builder(requireContext())

                        val inflatedView = LayoutInflater.from(requireContext())
                            .inflate(R.layout.dialog_note,
                                view as ViewGroup,
                                false)

                        val medicineInputLayout = inflatedView
                            .findViewById<TextInputLayout>(R.id.medicine_note_input_layout)
                        medicineInputLayout.visibility = View.GONE

                        val personNameInput =
                            inflatedView.findViewById<TextInputEditText>(R.id.note_person_input) as EditText
                        item.personName?.let {
                            personNameInput.setText(it)
                        }

                        val descriptionInput =
                            inflatedView.findViewById<TextInputEditText>(R.id.note_description_input) as EditText
                        descriptionInput.setText(item.description)

                        noteBuilder.setTitle("Редагувати нотаток")
                            .setView(inflatedView)
                            .setPositiveButton("Редагувати") { dialog, _ ->
                                if(descriptionInput.text.isNotBlank()) {
                                    dialog.dismiss()
                                    hideKeyboard()
                                    val name = if(personNameInput.text.isBlank()) {
                                        null
                                    } else {
                                        personNameInput.text.toString()
                                    }
                                    viewModel.updateNote(
                                        Note(
                                            item.id,
                                            name,
                                            descriptionInput.text.toString(),
                                            item.medicineId
                                        )
                                    )
                                }
                            }
                            .setNegativeButton("Скасувати") { dialog, _ ->
                                dialog.cancel()
                            }
                            .show()
                    }
                    1 -> {
                        val deleteBuilder = AlertDialog.Builder(requireContext())

                        deleteBuilder.setTitle("Видалити нотаток?")
                            .setPositiveButton("Видалити") { dialog, _ ->
                                dialog.dismiss()
                                viewModel.deleteNote(item)
                            }
                            .setNegativeButton("Скасувати") { dialog, _ -> dialog.cancel() }
                            .show()
                    }
                }
            }.show()
    }
}
