package com.oliinyk.medkit.ui.help

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.FragmentHelpBinding
import com.oliinyk.medkit.databinding.FragmentMapBinding
import com.oliinyk.medkit.viewmodel.help.HelpViewModel

class HelpFragment : Fragment() {

    private var _binding: FragmentHelpBinding? = null

    private val binding get() = _binding!!

    private lateinit var viewModel: HelpViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val viewModel =
            ViewModelProvider(this)[HelpViewModel::class.java]

        _binding = FragmentHelpBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}