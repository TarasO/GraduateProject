package com.oliinyk.medkit.ui.listener

interface OnItemClickListener {
    fun onItemClick(position: Int)
}