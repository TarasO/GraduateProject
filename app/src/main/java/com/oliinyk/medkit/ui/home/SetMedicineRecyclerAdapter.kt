package com.oliinyk.medkit.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.oliinyk.medkit.R
import com.oliinyk.medkit.model.MedicineState.*
import com.oliinyk.medkit.model.tuple.MedicineWithExpiration
import com.oliinyk.medkit.ui.listener.OnItemClickListener
import com.oliinyk.medkit.ui.listener.OnItemSelectedListener
import java.time.LocalDate
import java.time.temporal.ChronoUnit.DAYS

class SetMedicineRecyclerAdapter(private val context: Context,
                                 private val dataSet: List<MedicineWithExpiration>,
                                 private val onItemSelectedListener:
                                    OnItemSelectedListener<MedicineWithExpiration>):
    RecyclerView.Adapter<SetMedicineRecyclerAdapter.ViewHolder>(), OnItemClickListener {

    class ViewHolder(view: View,
                     private val onItemClickListener:
                     OnItemClickListener
    ): RecyclerView.ViewHolder(view), View.OnClickListener {
        val itemName: TextView = view.findViewById(R.id.set_medicine_name)
        val itemReleaseForm: TextView = view.findViewById(R.id.set_medicine_release_form)
        val itemProducer: TextView = view.findViewById(R.id.set_medicine_producer)
        val stateImage: ImageView = view.findViewById(R.id.set_medicine_state_image)
        val state: TextView = view.findViewById(R.id.set_medicine_state)

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onItemClickListener.onItemClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_set_medicine, parent, false)

        return ViewHolder(view, this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataSet[position]
        holder.itemName.text = context.getString(R.string.medicine_display_name, item.name)
        if(item.releaseFormName != null) {
            holder.itemReleaseForm.text = context
                .getString(R.string.results_item_release_form, item.releaseFormName)
        } else {
            holder.itemReleaseForm.visibility = View.GONE
        }
        if(item.producerName != null) {
            holder.itemProducer.text = context.getString(R.string.set_medicine_producer, item.producerName)
        } else {
            holder.itemProducer.visibility = View.GONE
        }

        when(item.state) {
            EMPTY -> {
                val drawable = AppCompatResources
                    .getDrawable(context, R.drawable.ic_circle_exclamation_solid_dark)
                holder.stateImage.setImageDrawable(drawable)
                holder.state.setTextColor(context.getColor(R.color.empty))
                holder.state.text = "Потрібне поповнення"
            }
            STOCKED -> {
                val drawable = AppCompatResources
                    .getDrawable(context, R.drawable.ic_circle_check_solid)
                holder.stateImage.setImageDrawable(drawable)
                holder.state.setTextColor(context.getColor(R.color.stocked))
                holder.state.text = "Поповнено"
            }
            EXPIRING -> {
                val drawable = AppCompatResources
                    .getDrawable(context, R.drawable.ic_triangle_exclamation_solid)
                holder.stateImage.setImageDrawable(drawable)
                holder.state.setTextColor(context.getColor(R.color.expiring))
                val days = DAYS.between(LocalDate.now(), item.expirationDate)
                holder.state.text = context.getString(R.string.set_medicine_expiration,
                                                      days.toString(), "днів")
            }
            EXPIRED -> {
                val drawable = AppCompatResources
                    .getDrawable(context, R.drawable.ic_circle_exclamation_solid)
                holder.stateImage.setImageDrawable(drawable)
                holder.state.setTextColor(context.getColor(R.color.expired))
                holder.state.text = "Прострочено"
            }
        }

        holder.state
    }

    override fun getItemCount() = dataSet.size

    override fun onItemClick(position: Int) {
        onItemSelectedListener.onItemSelected(dataSet[position])
    }
}