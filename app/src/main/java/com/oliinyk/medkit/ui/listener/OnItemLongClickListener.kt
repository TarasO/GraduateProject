package com.oliinyk.medkit.ui.listener

interface OnItemLongClickListener {
    fun onItemLongClick(position: Int) : Boolean
}