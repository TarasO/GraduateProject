package com.oliinyk.medkit.ui.map

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory.HUE_BLUE
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.oliinyk.medkit.R
import com.oliinyk.medkit.databinding.ActivityFindPharmacyBinding
import com.oliinyk.medkit.viewmodel.map.ClosestPharmacyRouteViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ClosestPharmacyRouteActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var viewModel: ClosestPharmacyRouteViewModel
    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityFindPharmacyBinding
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this)[ClosestPharmacyRouteViewModel::class.java]

        binding = ActivityFindPharmacyBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    location?.let {
                        val myLocation = LatLng(it.latitude, it.longitude)
                        mMap.addMarker(
                            MarkerOptions().position(myLocation)
                                .title("Моє розташування")
                                .icon(BitmapDescriptorFactory.defaultMarker(HUE_BLUE))
                        )
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15F))

                        CoroutineScope(Dispatchers.IO).launch {
                            val directions = viewModel.findClosestPharmacy(it)

                            val coordinates = directions.first.first
                            withContext(Dispatchers.Main) {
                                mMap.addMarker(
                                    MarkerOptions()
                                        .position(LatLng(coordinates[1], coordinates[0]))
                                        .title(directions.first.second)
                                )

                                mMap.addPolyline(
                                    PolylineOptions()
                                        .addAll(directions.second)
                                        .color(0xFF641FE3.toInt())
                                )
                            }
                        }
                    }
                }
        }
    }
}