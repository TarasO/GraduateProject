package com.oliinyk.medkit.viewmodel.notes

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oliinyk.medkit.MedkitDatabase
import com.oliinyk.medkit.dao.MedicineDAO
import com.oliinyk.medkit.dao.NoteDAO
import com.oliinyk.medkit.model.Note
import com.oliinyk.medkit.model.relationship.MedicineWithNotes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NotesViewModel : ViewModel() {

    private val medicineDao: MedicineDAO
    private val noteDao: NoteDAO
    val medicines: LiveData<List<MedicineWithNotes>>

    init {
        medicineDao = MedkitDatabase.getDatabase().medicineDao()
        noteDao = MedkitDatabase.getDatabase().noteDao()
        medicines = medicineDao.findWithNotes()
    }

    fun addNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            noteDao.insertAll(note)
        }
    }

    fun updateNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            noteDao.update(note)
        }
    }

    fun deleteNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            noteDao.delete(note)
        }
    }

    fun deleteNotesByMedicineId(medicineId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            noteDao.deleteByMedicineId(medicineId)
        }
    }
}