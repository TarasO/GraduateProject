package com.oliinyk.medkit.viewmodel.map

import android.location.Location
import androidx.lifecycle.ViewModel
import com.oliinyk.medkit.util.MapUtils.latAddDistance
import com.oliinyk.medkit.util.MapUtils.lonAddDistance
import com.oliinyk.medkit.api.OpenStreetMapAPI

class FindPharmacyViewModel: ViewModel() {

    public fun findPharmaciesAround(location: Location): String {
        val s = latAddDistance(location.latitude, -2500.0)
        val w = lonAddDistance(location.longitude, location.latitude, -2500.0)
        val n = latAddDistance(location.latitude, 2500.0)
        val e = lonAddDistance(location.longitude, location.latitude, 2500.0)

        return OpenStreetMapAPI.findPharmaciesAround(s, w, n, e).body!!.string()
    }
}