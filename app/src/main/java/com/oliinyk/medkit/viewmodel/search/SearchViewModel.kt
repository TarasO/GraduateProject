package com.oliinyk.medkit.viewmodel.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.oliinyk.medkit.MedkitDatabase
import com.oliinyk.medkit.dao.CategoryDAO
import com.oliinyk.medkit.model.tuple.CategoryNameTuple

class SearchViewModel : ViewModel() {

    private val categoryDao: CategoryDAO
    val categories: LiveData<List<CategoryNameTuple>>

    init {
        categoryDao = MedkitDatabase.getDatabase().categoryDao()
        categories = categoryDao.findNames()
    }
}