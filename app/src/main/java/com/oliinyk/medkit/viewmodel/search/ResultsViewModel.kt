package com.oliinyk.medkit.viewmodel.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oliinyk.medkit.MedkitDatabase
import com.oliinyk.medkit.dao.MedicineDAO
import com.oliinyk.medkit.dao.SetMedicineDAO
import com.oliinyk.medkit.model.relationship.MedicineWithReleaseFormAndProducer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ResultsViewModel : ViewModel() {
    private val medicineDao: MedicineDAO
    private val setMedicineDao: SetMedicineDAO

    private val _medicines: MutableLiveData<List<MedicineWithReleaseFormAndProducer>>
    val medicines: LiveData<List<MedicineWithReleaseFormAndProducer>>

    var medicinesInSet: List<Long>? = null

    init {
        medicineDao = MedkitDatabase.getDatabase().medicineDao()
        setMedicineDao = MedkitDatabase.getDatabase().setMedicineDao()

        _medicines = MutableLiveData()
        medicines = _medicines
    }

    fun findByName(name: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _medicines.postValue(medicineDao.findByName("$name%"))
        }
    }

    fun findByInn(inn: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _medicines.postValue(medicineDao.findByInn("$inn%"))
        }
    }

    fun findBySubstanceName(substanceName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _medicines
                .postValue(medicineDao.findBySubstanceName("$substanceName%"))
        }
    }

    fun findByCategoryId(categoryId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            _medicines.postValue(medicineDao.findByCategoryId(categoryId))
        }
    }

    fun findMedicinesIdInSet(setId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            medicinesInSet = setMedicineDao.findBySetId(setId)
        }
    }
}