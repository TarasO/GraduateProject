package com.oliinyk.medkit.viewmodel.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oliinyk.medkit.MedkitDatabase
import com.oliinyk.medkit.dao.MedicineDAO
import com.oliinyk.medkit.dao.NoteDAO
import com.oliinyk.medkit.dao.SetMedicineDAO
import com.oliinyk.medkit.model.relationship.MedicineWithEverything
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MedicineDisplayViewModel : ViewModel() {

    private val medicineDao: MedicineDAO
    private val setMedicineDao: SetMedicineDAO
    private val noteDao: NoteDAO

    private val _medicineWithEverything: MutableLiveData<MedicineWithEverything>
    val medicineWithEverything: LiveData<MedicineWithEverything>

    var isInSet: Boolean = false
    var hasNotes: Boolean = false

    init {
        medicineDao = MedkitDatabase.getDatabase().medicineDao()
        setMedicineDao = MedkitDatabase.getDatabase().setMedicineDao()
        noteDao = MedkitDatabase.getDatabase().noteDao()

        _medicineWithEverything = MutableLiveData()
        medicineWithEverything = _medicineWithEverything
    }

    fun findMedicineById(id: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            _medicineWithEverything.postValue(medicineDao.findById(id))
        }
    }

    fun existsByMedicineIdAndSetId(medicineId: Long, setId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            isInSet = setMedicineDao.existsByMedicineIdAndSetId(medicineId, setId)
        }
    }

    fun hasNotesByMedicineId(medicineId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            hasNotes = noteDao.hasNotesByMedicineId(medicineId)
        }
    }
}