package com.oliinyk.medkit.viewmodel.car

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oliinyk.medkit.MedkitDatabase
import com.oliinyk.medkit.dao.MedkitSetDAO
import com.oliinyk.medkit.dao.SetMedicineDAO
import com.oliinyk.medkit.model.*
import com.oliinyk.medkit.model.relationship.SetWithMedicines
import com.oliinyk.medkit.util.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate

class CarViewModel : ViewModel() {

    private val setDao: MedkitSetDAO
    private val setMedicineDAO: SetMedicineDAO
    val sets: LiveData<List<SetWithMedicines>>

    init {
        setDao = MedkitDatabase.getDatabase().medkitSetDao()
        setMedicineDAO = MedkitDatabase.getDatabase().setMedicineDao()
        sets = setDao.getSetsWithMedicines(SetType.CAR)
    }

    fun addSet(set: MedkitSet, type: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val id = setDao.insert(set)
            val list = ArrayList<SetMedicine>()
            when(type) {
                0 -> {
                    for(i in -100 downTo -117) {
                        list.add(SetMedicine(id, i.toLong(),
                            null, MedicineState.STOCKED))
                    }
                }
                1 -> {
                    for(i in -200 downTo -218) {
                        list.add(SetMedicine(id, i.toLong(),
                            null, MedicineState.STOCKED))
                    }
                }
            }
            setMedicineDAO.insertAll(*list.toTypedArray())
        }
    }

    fun updateSet(set: MedkitSet) {
        viewModelScope.launch(Dispatchers.IO) {
            setDao.updateSet(set)
        }
    }

    fun deleteSet(set: MedkitSet) {
        viewModelScope.launch(Dispatchers.IO) {
            setDao.deleteSet(set)
        }
    }

    fun addMedicineToSet(set: MedkitSet, medicine: Medicine, expirationDate: LocalDate) {
        val state = Utils.getMedicineState(expirationDate)

        val setMedicine = SetMedicine(set.id, medicine.id, expirationDate, state)
        viewModelScope.launch(Dispatchers.IO) {
            setMedicineDAO.insertAll(setMedicine)
        }
    }

    fun removeMedicineFromSet(setId: Long, medicineId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            setMedicineDAO.delete(setId, medicineId)
        }
    }

    fun updateMedicineState(setId: Long, medicineId: Long, state: MedicineState) {
        viewModelScope.launch(Dispatchers.IO) {
            setMedicineDAO.updateMedicineState(setId, medicineId, state)
        }
    }

    fun updateMedicineExpirationDate(setId: Long, medicineId: Long, expirationDate: LocalDate) {
        val state = Utils.getMedicineState(expirationDate)
        val set = SetMedicine(setId, medicineId, expirationDate, state)
        viewModelScope.launch(Dispatchers.IO) {
            setMedicineDAO.update(set)
        }
    }
}