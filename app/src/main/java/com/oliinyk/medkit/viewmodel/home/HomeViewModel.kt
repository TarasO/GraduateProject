package com.oliinyk.medkit.viewmodel.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oliinyk.medkit.MedkitDatabase
import com.oliinyk.medkit.dao.MedkitSetDAO
import com.oliinyk.medkit.dao.SetMedicineDAO
import com.oliinyk.medkit.model.Medicine
import com.oliinyk.medkit.model.MedicineState
import com.oliinyk.medkit.model.MedkitSet
import com.oliinyk.medkit.model.SetMedicine
import com.oliinyk.medkit.model.SetType.HOME
import com.oliinyk.medkit.model.relationship.SetWithMedicines
import com.oliinyk.medkit.util.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate

class HomeViewModel : ViewModel() {

    private val setDao: MedkitSetDAO
    private val setMedicineDAO: SetMedicineDAO
    val sets: LiveData<List<SetWithMedicines>>

    init {
        setDao = MedkitDatabase.getDatabase().medkitSetDao()
        setMedicineDAO = MedkitDatabase.getDatabase().setMedicineDao()
        sets = setDao.getSetsWithMedicines(HOME)
    }

    fun addSet(set: MedkitSet) {
        viewModelScope.launch(Dispatchers.IO) {
            setDao.insertAll(set)
        }
    }

    fun updateSet(set: MedkitSet) {
        viewModelScope.launch(Dispatchers.IO) {
            setDao.updateSet(set)
        }
    }

    fun deleteSet(set: MedkitSet) {
        viewModelScope.launch(Dispatchers.IO) {
            setDao.deleteSet(set)
        }
    }

    fun addMedicineToSet(set: MedkitSet, medicine: Medicine, expirationDate: LocalDate) {
        val state = Utils.getMedicineState(expirationDate)

        val setMedicine = SetMedicine(set.id, medicine.id, expirationDate, state)
        viewModelScope.launch(Dispatchers.IO) {
            setMedicineDAO.insertAll(setMedicine)
        }
    }

    fun removeMedicineFromSet(setId: Long, medicineId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            setMedicineDAO.delete(setId, medicineId)
        }
    }

    fun updateMedicineState(setId: Long, medicineId: Long, state: MedicineState) {
        viewModelScope.launch(Dispatchers.IO) {
            setMedicineDAO.updateMedicineState(setId, medicineId, state)
        }
    }

    fun updateMedicineExpirationDate(setId: Long, medicineId: Long, expirationDate: LocalDate) {
        val state = Utils.getMedicineState(expirationDate)
        val set = SetMedicine(setId, medicineId, expirationDate, state)
        viewModelScope.launch(Dispatchers.IO) {
            setMedicineDAO.update(set)
        }
    }
}