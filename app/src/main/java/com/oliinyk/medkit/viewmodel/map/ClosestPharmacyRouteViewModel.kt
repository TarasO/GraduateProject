package com.oliinyk.medkit.viewmodel.map

import android.location.Location
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.oliinyk.medkit.api.OpenStreetMapAPI
import com.oliinyk.medkit.util.MapUtils

class ClosestPharmacyRouteViewModel: ViewModel() {

    public fun findClosestPharmacy(location: Location): Pair<Pair<Array<Double>, String?>, MutableList<LatLng>> {
        var locations: MutableMap<Array<Double>, String>?
        var distance = 2500.0

        do {
            val s = MapUtils.latAddDistance(location.latitude, -distance)
            val w = MapUtils.lonAddDistance(location.longitude, location.latitude, -distance)
            val n = MapUtils.latAddDistance(location.latitude, distance)
            val e = MapUtils.lonAddDistance(location.longitude, location.latitude, distance)

            locations = MapUtils
                .jsonToLatLng(OpenStreetMapAPI.findPharmaciesAround(s, w, n, e).body!!.string())
            distance += 1000
        } while (locations.isNullOrEmpty())

        locations[arrayOf(location.longitude, location.latitude)] = "Моє розташування"

        val distances = MapUtils
            .jsonToDistances(OpenStreetMapAPI.getDistanceMatrix(locations).body!!.string())
        val closestLocation = distances.minWithOrNull { t, t2 -> t2.value.compareTo(t.value) }!!.key

        val directions = MapUtils
            .jsonToDirections(OpenStreetMapAPI.getDirections(location, closestLocation).body!!.string())

        val pair = Pair(closestLocation, locations[closestLocation])
        return Pair(pair, directions)
    }
}