package com.oliinyk.medkit.model

enum class SetType {
    STUB, CAR, HOME
}