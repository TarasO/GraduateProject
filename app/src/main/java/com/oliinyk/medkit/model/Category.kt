package com.oliinyk.medkit.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.oliinyk.medkit.converters.LocalDateTimeConverter
import java.time.LocalDateTime

@Entity
data class Category(
    @PrimaryKey
    val id: Long,
    val name: String,
    @TypeConverters(LocalDateTimeConverter::class)
    val createdAt: LocalDateTime,
    @TypeConverters(LocalDateTimeConverter::class)
    val updatedAt: LocalDateTime?
) {
    constructor(id: Long, name: String, createdAt: LocalDateTime):
            this(id, name, createdAt, null)
}