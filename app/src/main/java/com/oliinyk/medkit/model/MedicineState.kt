package com.oliinyk.medkit.model

enum class MedicineState {
    EMPTY, STOCKED, EXPIRING, EXPIRED
}