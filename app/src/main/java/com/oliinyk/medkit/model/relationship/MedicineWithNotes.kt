package com.oliinyk.medkit.model.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.oliinyk.medkit.model.Note
import com.oliinyk.medkit.model.tuple.MedicineInfo

data class MedicineWithNotes(
    @Embedded
    val medicine: MedicineInfo,
    @Relation(
        parentColumn = "id",
        entityColumn = "medicineId",
    )
    val notes: List<Note>
)
