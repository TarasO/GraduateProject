package com.oliinyk.medkit.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.TypeConverters
import com.oliinyk.medkit.converters.LocalDateConverter
import java.time.LocalDate

@Entity(primaryKeys = ["setId", "medicineId"])
data class SetMedicine(
    val setId: Long,
    @ColumnInfo(index = true)
    val medicineId: Long,
    @TypeConverters(LocalDateConverter::class)
    val expirationDate: LocalDate?,
    val state: MedicineState
)