package com.oliinyk.medkit.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.oliinyk.medkit.converters.LocalDateTimeConverter
import java.time.LocalDateTime

@Entity
data class Producer(
    @PrimaryKey
    val id: Long,
    val name: String,
    val address: String?,
    val countryId: Long,
    @TypeConverters(LocalDateTimeConverter::class)
    val createdAt: LocalDateTime,
    @TypeConverters(LocalDateTimeConverter::class)
    val updatedAt: LocalDateTime?
) {
    constructor(id: Long, name: String, address: String, countryId: Long, createdAt: LocalDateTime):
            this(id, name, address, countryId, createdAt, null)
}