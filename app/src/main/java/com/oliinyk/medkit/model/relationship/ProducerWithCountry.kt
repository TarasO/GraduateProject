package com.oliinyk.medkit.model.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.oliinyk.medkit.model.Country
import com.oliinyk.medkit.model.Producer

data class ProducerWithCountry(
    @Embedded
    val producer: Producer,
    @Relation(parentColumn = "countryId",
              entityColumn = "id")
    val country: Country
)
