package com.oliinyk.medkit.model.tuple

import androidx.room.ColumnInfo
import java.io.Serializable

data class CategoryNameTuple(
    @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "name") val name: String
): Serializable