package com.oliinyk.medkit.model.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.oliinyk.medkit.model.MedkitSet
import com.oliinyk.medkit.model.tuple.MedicineWithExpiration

data class SetWithMedicines(
    @Embedded
    val medkitSet: MedkitSet,
    @Relation(
        parentColumn = "id",
        entityColumn = "setId"
    )
    val medicines: List<MedicineWithExpiration>
)