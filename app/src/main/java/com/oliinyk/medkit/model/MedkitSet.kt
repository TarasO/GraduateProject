package com.oliinyk.medkit.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class MedkitSet(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    @NonNull
    val name: String,
    val setType: SetType
) {
    constructor(name: String, setType: SetType): this(0, name, setType)
}