package com.oliinyk.medkit.model.relationship

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.oliinyk.medkit.model.*

data class MedicineWithEverything(
    @Embedded
    val medicine: Medicine,
    @Relation(
        parentColumn = "releaseFormId",
        entityColumn = "id",
    )
    val releaseForm: ReleaseForm,
    @Relation(
        parentColumn = "id",
        entityColumn = "id",
        associateBy = Junction(
            MedicineActiveSubstance::class,
            parentColumn = "medicineId",
            entityColumn = "activeSubstanceId")
    )
    val substances: List<ActiveSubstance>,
    @Relation(
        parentColumn = "id",
        entityColumn = "id",
        associateBy = Junction(
            CategoryMedicine::class,
            parentColumn = "medicineId",
            entityColumn = "categoryId")
    )
    val categories: List<Category>,
    @Relation(
        entity = Producer::class,
        parentColumn = "producerId",
        entityColumn = "id"
    )
    val producerWithCountry: ProducerWithCountry
)
