package com.oliinyk.medkit.model

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(primaryKeys = ["medicineId", "categoryId"])
data class CategoryMedicine(
    val medicineId: Long,
    @ColumnInfo(index = true)
    val categoryId: Long
)