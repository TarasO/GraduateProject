package com.oliinyk.medkit.model

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(primaryKeys = ["medicineId", "activeSubstanceId"])
class MedicineActiveSubstance(
    val medicineId: Long,
    @ColumnInfo(index = true)
    val activeSubstanceId: Long
)