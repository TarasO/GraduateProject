package com.oliinyk.medkit.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Note (
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val personName: String?,
    @NonNull
    val description: String,
    @NonNull
    val medicineId: Long
) {
    constructor(personName: String?, description: String, medicineId: Long):
            this(0, personName, description, medicineId)
}