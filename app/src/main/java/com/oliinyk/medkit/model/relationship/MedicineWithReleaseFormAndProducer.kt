package com.oliinyk.medkit.model.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.oliinyk.medkit.model.Medicine
import com.oliinyk.medkit.model.Producer
import com.oliinyk.medkit.model.ReleaseForm

data class MedicineWithReleaseFormAndProducer(
    @Embedded
    val medicine: Medicine,
    @Relation(
        parentColumn = "releaseFormId",
        entityColumn = "id",
    )
    val releaseForm: ReleaseForm,
    @Relation(
        entity = Producer::class,
        parentColumn = "producerId",
        entityColumn = "id"
    )
    val producerWithCountry: ProducerWithCountry
)
