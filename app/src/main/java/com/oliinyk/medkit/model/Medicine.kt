package com.oliinyk.medkit.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.oliinyk.medkit.converters.LocalDateTimeConverter
import java.io.Serializable
import java.time.LocalDateTime

@Entity
data class Medicine(
    @PrimaryKey
    val id: Long,
    val name: String,
    val inn: String?,
    val expirationPeriod: Int?,
    val byRecipe: Boolean,
    val registrationNumber: String?,
    val manualUrl: String?,
    val producerId: Long?,
    val releaseFormId: Long?,
    @TypeConverters(LocalDateTimeConverter::class)
    val createdAt: LocalDateTime?,
    @TypeConverters(LocalDateTimeConverter::class)
    val updatedAt: LocalDateTime?
): Serializable {
    constructor(id: Long, name: String, inn: String, expirationPeriod: Int, byRecipe: Boolean,
                registrationNumber: String, manualUrl: String?, producerId: Long,
                releaseFormId: Long, createdAt: LocalDateTime):
            this(id, name, inn, expirationPeriod, byRecipe, registrationNumber, manualUrl,
                producerId, releaseFormId, createdAt, null)

    constructor(id: Long, name: String):
        this(id, name, null, null, false, null,
            null, null, null, null, null)
}