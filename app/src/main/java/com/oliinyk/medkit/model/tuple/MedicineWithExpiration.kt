package com.oliinyk.medkit.model.tuple

import androidx.room.DatabaseView
import com.oliinyk.medkit.model.MedicineState
import java.time.LocalDate

@DatabaseView("SELECT m.id, sm.setId, m.name, m.expirationPeriod, p.name AS producerName, " +
        "rf.name AS releaseFormName, sm.expirationDate, sm.state FROM Medicine m " +
        "LEFT JOIN Producer p ON m.producerId = p.id LEFT JOIN ReleaseForm rf " +
        "ON m.releaseFormId = rf.id INNER JOIN SetMedicine sm ON m.id = sm.medicineId")
data class MedicineWithExpiration(
   val id: Long,
   val setId: Long,
   val name: String,
   val expirationPeriod: Int?,
   val producerName: String?,
   val releaseFormName: String?,
   val expirationDate: LocalDate?,
   val state: MedicineState
)