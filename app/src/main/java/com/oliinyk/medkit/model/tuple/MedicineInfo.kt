package com.oliinyk.medkit.model.tuple

import androidx.room.DatabaseView

@DatabaseView("SELECT DISTINCT m.id, m.name, p.name AS producerName, rf.name " +
        "AS releaseFormName FROM Medicine m INNER JOIN Producer p ON m.producerId = p.id " +
        "INNER JOIN ReleaseForm rf ON m.releaseFormId = rf.id INNER JOIN Note n " +
        "ON m.id = n.medicineId")
data class MedicineInfo (
    val id: Long,
    val name: String,
    val producerName: String,
    val releaseFormName: String
)